var fs = require('fs');
var path = require('path');

var list = [];

var name = (file) => {
    return path.basename(file, '.json');
};

fs.readdirSync(path.join(__dirname, '..', 'data')).forEach(file => {
    list.push(name(file));
});


module.exports = list || [];
