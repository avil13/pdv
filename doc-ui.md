### Разбор документации и сравнение с примером:
https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382

---
`document`:
- [✔] содержит: `documentId`
- [x] должно содержать, но они выделены как самостоятельные поля: `flow`, `state`
- [x] должно содержать, но нет вообще: `templateId`, `srcDocumentId`

---



---
>  `screens` - основной элемент для построения форм - не имеет документации

> `events` - не имеет документации

> `references` - не имеет документации

---
body ->
[output](https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382#id-ПротоколобменаСБОЛUIWorkflow-WorkflowOutputType)->
[screens](https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382#id-ПротоколобменаСБОЛUIWorkflow-ScreenType) -> 
[widgets](https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382#id-ПротоколобменаСБОЛUIWorkflow-WidgetType) -> 
[fields](https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382#id-ПротоколобменаСБОЛUIWorkflow-FieldType)

---

`fields` - является одним из основных состовляющих согласно документации. 
[Описание полей](https://sbtatlas.sigma.sbrf.ru/wiki/pages/viewpage.action?pageId=96993382#id-ПротоколобменаСБОЛUIWorkflow-FieldType)

Исходя из примера является полем виджета.


---




