var express = require('express');
var app = express();

var fs = require('fs');
var path = require('path');
var open = require('open');

var port = 3000;
var list = require('./libs/list-files');

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'));


var callback = function (req, res) {
    const id = parseInt(req.params.id);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Content-Type', 'application/json');

    try {
        const file = path.normalize(__dirname + `/data/${id}.json`);
        let data = fs.readFileSync(file, 'utf8');

        res.send(data);
    } catch (e) {
        res.send('Page not found');
        res.status(404).end();
    }
};

app.get('/', function (req, res) {
    res.render('index', {
        list: list.map((v) => `/data/${v}.json`)
    });
});


app.get('/data/:id', callback);
app.post('/data/:id', callback);


app.listen(port, function () {
    console.log(`App running on http://localhost:${port}/`);
    if (process.argv.indexOf('open') > -1) {
        setTimeout(() => {
            open(`http://localhost:${port}/`);
        }, 2000);
    }
});
